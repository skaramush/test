import Vue from 'vue'
import Router from 'vue-router'
import mainPage from '@/components/main-page'
import adminState from '@/components/admin-state'
import profilePage from '@/components/profilePage'
import tableFloor from '@/components/table-floor'
import statisticFloor from '@/components/statistic-floor'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'mainPage',
      component: mainPage
    },
    {
      path: '/admin',
      name: 'admin-state',
      component: adminState,
      beforeEnter: guardRoute
    },
    {
      path: '/profile',
      name: 'profile-page',
      component: profilePage,
      beforeEnter: guardRoute
    },
    {
      path: '/table-floor/:id',
      name: 'table-floor',
      component: tableFloor,
      beforeEnter: guardRoute
    },
    {
      path: '/statistic-floor/:floorId',
      name: 'statistic-floor',
      component: statisticFloor,
      beforeEnter: guardRoute
    }
  ]
})

function guardRoute(to, from, next) {
  if (localStorage.token) {
    next()
  } else {
    next({
      name: 'mainPage'
    })
  }
}
