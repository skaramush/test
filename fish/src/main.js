// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import BootstrapVue from 'bootstrap-vue'
import VueResource from 'vue-resource'
import Auth from './auth'
import store from './store'
import VModal from 'vue-js-modal'

Vue.use(VModal, { dynamic: true })
Vue.use(BootstrapVue);
Vue.use(VueResource);
Vue.config.productionTip = false;
Vue.use(Auth);

Vue.http.interceptor.before = function(request) {
  request.headers.set('Authorization', 'Bearer ' + localStorage.token);
};

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
