// Initializes the `renters` service on path `/renters`
const createService = require('feathers-sequelize');
const createModel = require('../../models/renters.model');
const hooks = require('./renters.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'renters',
    Model,
    paginate: false
  };

  // Initialize our service with any options it requires
  app.use('/renters', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('renters');

  service.hooks(hooks);
};
