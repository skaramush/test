// Initializes the `renters-type` service on path `/renters-type`
const createService = require('feathers-sequelize');
const createModel = require('../../models/renters-type.model');
const hooks = require('./renters-type.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'renters-type',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/renters-type', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('renters-type');

  service.hooks(hooks);
};
