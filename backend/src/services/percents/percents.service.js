// Initializes the `percents` service on path `/percents`
const createService = require('feathers-sequelize');
const createModel = require('../../models/percents.model');
const hooks = require('./percents.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'percents',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/percents', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('percents');

  service.hooks(hooks);
};
