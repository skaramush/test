// Initializes the `market` service on path `/market`
const createService = require('feathers-sequelize');
const createModel = require('../../models/market.model');
const hooks = require('./market.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'market',
    Model,
    paginate: false
  };

  // Initialize our service with any options it requires
  app.use('/market', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('market');

  service.hooks(hooks);
};
