const { authenticate } = require('@feathersjs/authentication').hooks;
// var xlsx = require('node-xlsx').default;
//
// const data = [[1, 2, 3], [true, false, null, 'sheetjs'], ['foo', 'bar', new Date('2014-02-19T14:30Z'), '0.3'], ['baz', null, 'qux']];
// var buffer = xlsx.build([{name: "mySheetName", data: data}]);


const fs = require ('fs');
const NodeXls = require('node-xls');

module.exports = {
  before: {
    all: [ authenticate('jwt') ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [
      hook => {
        return hook.app.service('renters').find({
          query: {}
        }).then(res => {
          const data = [];
          const promiseArr = [];
          const statusDictionary = {
            free: 'Свободен',
            reserve: 'В резерве',
            busy: 'Занят'
          };

          res.forEach(item => {
            data.push(
              {
                id: item.id,
                name: item.name,
                place: item.place,
                status: statusDictionary[item.status],
                renter: item.renter,
                date_from: item.date_from,
                date_to: item.date_to,
                area: item.area,
                floor: item.floor
              }
            );
          });

          data.forEach(secondItem => {
            promiseArr.push(hook.app.service('market').find({
              query: {
                place_id: secondItem.name,
                date: hook.params.query.mainDate
              }
            }))
          });

          return Promise.all(promiseArr).then(res => {
            res[0].data.forEach(itemMarket => {

              const correctObj = data.findIndex(x => {
                return x.name == itemMarket.place_id
              });

              data[correctObj] = Object.assign(data[correctObj], itemMarket);
            });

            // { id: 4,
            //   name: '123',
            //   place: null,
            //   status: 'busy',
            //   renter: null,
            //   doc_number: null,
            //   area: '11.11',
            //   floor: '2',
            //   place_id: '123',
            //   price_base: '234',
            //   price_ekp: '234',
            //   additional_pay: '234',
            //   price_var: '234',
            //   percentPay: '234',
            //   market: '234',
            //   date: '08.2018',
            //   createdAt: 2018-08-06T19:18:54.000Z,
            //   updatedAt: 2018-08-08T06:20:11.000Z },


            const tool = new NodeXls();
            const xls = tool.json2xls(data, {
              order: ['id', 'name', 'place', 'status', 'renter', 'area', 'floor', 'price_base', 'price_ekp', 'additional_pay', 'price_var', 'percentPay', 'market', 'date_from', 'date_to'],
              fieldMap: {
                name: "Бти",
                place: 'Номер',
                status: 'Статус',
                renter: 'Арендатор',
                area: 'Площадь',
                floor: 'Этаж',
                price_base: 'Базовая стоимость',
                price_ekp: 'Эксплатыционная',
                additional_pay: 'Доп доход',
                price_var: 'Переменная',
                percentPay: 'Процент с продаж',
                market: 'Маркетинговый сбор',
                date_from: 'Договор с',
                date_to: 'Договор по',
              }
            });


            fs.writeFileSync('./public/output.xlsx', xls, 'binary');

            hook.result = '/output.xlsx';
            return hook;

            // hook.result = data;
            // return hook;
          })


        });
      }
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
