// Initializes the `statistic` service on path `/statistic`
const createService = require('./statistic.class.js');
const hooks = require('./statistic.hooks');

module.exports = function (app) {
  
  const paginate = app.get('paginate');

  const options = {
    name: 'statistic',
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/statistic', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('statistic');

  service.hooks(hooks);
};
