// Initializes the `download` service on path `/download`
const createService = require('./download.class.js');
const hooks = require('./download.hooks');
const fs = require ('fs');
const XLSX = require('node-xlsx');
var path = require('path');

module.exports = function (app) {
  
  const paginate = app.get('paginate');

  const options = {
    name: 'download',
    paginate
  };

  // Initialize our service with any options it requires
  // app.use('/download', createService(options), (req, res) => {
  //   var filePath = 'output.xlsx';
  // //   var stat = fs.statSync(filePath);
  // //
  // //   res.writeHead(200, {
  // //     "Content-Type": "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
  // //     'Content-Length': stat.size
  // //   });
  // //
  // //   var readStream = fs.createReadStream(filePath);
  // //
  // //   readStream.pipe(res);
  //
  //   // var fileName = "output.xlsx";
  //   // res.setHeader('Content-disposition', 'attachment; filename=' + fileName);
  //   // res.setHeader('Content-type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
  //   // // var wbout = XLSX.write(workbook.finalize(), { bookType: 'xlsx', type: 'buffer'});
  //   // var readStream = fs.createReadStream(filePath);
  //   // res.send(readStream);Скачать
  //
  //
  //   const buffer = FS.readFileSync('./output.xlsx')
  //   res.json(buffer)
  // });

  // app.get('/download',(req,res) => {
  //     res.setHeader('Content-disposition', 'attachment; filename=' + 'fileName');
  //     res.setHeader('Content-type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
  //   res.sendFile('/output.xlsx')
  // })

  // app.get('/download',(req,res, next) => {
  //   res.sendFile('/package.json');
  // });


  // Get our initialized service so that we can register hooks and filters
  const service = app.service('download');

};
