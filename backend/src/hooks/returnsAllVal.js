const logger = require('winston');

module.exports = function () {
  return hook => {
    const data = hook.result.data;
    const promiseArr = [];

    if (data.length) {
      data.forEach(item => {
        promiseArr.push(hook.app.service('market').find({
          query: {
            place_id: item.name
          }
        }))
      })
    }

    return Promise.all(promiseArr).then(res => {
      res[0].data.forEach(item => {

        const correctObj = data.findIndex(x => {
          return x.name == item.place_id
        });

        data[correctObj] = Object.assign(data[correctObj], item);
      });

      hook.result = data;
      return hook;
    })
  }
};