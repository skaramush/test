const logger = require('winston');

module.exports = function () {
  return hook => {

    return hook.app.service('percents').find({
      query: {
        place_id: hook.data.name,
        date: hook.data.percentPayDate
      }
    }).then(res => {
      const resArr = res.data;

      if (resArr.length) {
        hook.app.service('percents').patch(resArr[0].id, {
          value: hook.data.percentPay
        }).then(() => {
          return hook;
        })
      } else {
        hook.app.service('percents').create({
          place_id: hook.data.name,
          date: hook.data.percentPayDate,
          value: hook.data.percentPay
        }).then(() => {
          return hook
        })
      }
    });
  }
};