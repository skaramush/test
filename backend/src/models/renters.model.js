// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const renters = sequelizeClient.define('renters', {
    name: {
      type: DataTypes.STRING
    },
    place: {
      type: DataTypes.STRING
    },
    status: {
      type: DataTypes.STRING
    },
    renter: {
      type: DataTypes.STRING
    },
    renter_type: {
      type: DataTypes.STRING
    },
    doc_number: {
      type: DataTypes.STRING
    },
    date_from: {
      type: DataTypes.DATE
    },
    date_to: {
      type: DataTypes.DATE
    },
    price_base: {
      type: DataTypes.INTEGER
    },
    price_var: {
      type: DataTypes.INTEGER
    },
    price_ekp: {
      type: DataTypes.INTEGER
    },
    area: {
      type: DataTypes.STRING
    },
    floor: {
      type: DataTypes.INTEGER
    },
    image_id: {
      type: DataTypes.STRING
    },
    showOnPlane: {
      type: DataTypes.BOOLEAN
    },
    showOnPlaneArea: {
      type: DataTypes.BOOLEAN
    },
    showOnPlaneCompanyName: {
      type: DataTypes.BOOLEAN
    },
    placesType: {
      type: DataTypes.STRING
    }
  }, {
    hooks: {
      beforeCount(options) {
        options.raw = true;
      }
    }
  });

  renters.associate = function (models) { // eslint-disable-line no-unused-vars
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
  };

  return renters;
};
