// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const percents = sequelizeClient.define('percents', {
    place_id: {
      type: DataTypes.STRING
    },
    value: {
      type: DataTypes.STRING
    },
    date: {
      type: DataTypes.STRING // не спрашивай меня почему так
    },
  }, {
    hooks: {
      beforeCount(options) {
        options.raw = true;
      }
    }
  });

  percents.associate = function (models) { // eslint-disable-line no-unused-vars
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
  };

  return percents;
};
