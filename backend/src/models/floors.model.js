// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const floors = sequelizeClient.define('floors', {
    number: {
      type: DataTypes.INTEGER
    },
    busy: {
      type: DataTypes.INTEGER
    },
    free: {
      type: DataTypes.INTEGER
    },
    reserve: {
      type: DataTypes.INTEGER
    },
    count: {
      type: DataTypes.INTEGER
    }
  }, {
    hooks: {
      beforeCount(options) {
        options.raw = true;
      }
    }
  });

  floors.associate = function (models) { // eslint-disable-line no-unused-vars
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
  };

  return floors;
};
